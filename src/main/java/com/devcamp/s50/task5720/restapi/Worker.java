package com.devcamp.s50.task5720.restapi;

public class Worker extends Person {
    private int salary;

    public Worker(int age, String gender, String name, Address address) {
        super(age, gender, name, address);
    }
    public Worker(int age, String gender, String name, Address address, int salary) {
        super(age, gender, name, address);
        this.salary = salary;
    }
    @Override
    public void eat(){
        System.out.println("Worker is eating");
    }
    public String Working(){
        return new String("Worker is working");
    }
    public int getSalary() {
        return salary;
    }
    public void setSalary(int salary) {
        this.salary = salary;
    }
    
}
