package com.devcamp.s50.task5720.restapi;
import java.util.ArrayList;
import java.util.Arrays;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class PersonController {
    @CrossOrigin
    @GetMapping("/listStudent")
    public ArrayList<Student> getListStudent(){
        ArrayList<Student> students = new ArrayList<Student>();
        Subject subjectHoaPhanTich = new Subject ("Hóa Phân Tích", 1, new Professor(32, "male", "Minh Tuấn", new Address()));
        ArrayList<Subject> subjects01 = new ArrayList<>();
        subjects01.add(subjectHoaPhanTich);
        Student studentGiang = new Student(28, "female", "Giang", new Address(), 1, subjects01);
        Student studentLinh = new Student(18, "female", "Linh", new Address(), 2, new ArrayList<Subject>(){
            {
                add(new Subject("Tiếng Anh cơ bản",2, new Professor(45,"male", "Anh Quân", new Address())));
                add(new Subject("Toán cao cấp",3, new Professor(40,"female", "Thu Trang", new Address())));
            }
        });
        Student studentTrinh = new Student(22, "female", "Trinh", new Address(), 3, 
                new ArrayList<Subject>(Arrays.asList(
                    new Subject("Tiếng Anh chuyên ngành",4, new Professor(42,"male", "Hoàng Hải", new Address())),
                    new Subject("Mác- Lênin",5, new Professor(40,"female", "Thu Hoa", new Address()))
                )));
        students.add(studentGiang);
        students.add(studentLinh);
        students.add(studentTrinh);
        return students;
    }
}
